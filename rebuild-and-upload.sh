#!/usr/bin/env bash

# ###########################################################################
# GET PARAMETERS

source alfresco_settings.sh


# ###########################################################################
# FEATURE FLAGS

[ -z "$TIMESTAMP" ] && TIMESTAMP=$(date +%Y%m%d_%H%M)

DO_CLEAN=${DO_CLEAN:-1}
DO_NPM=${DO_NPM:-1}
DO_YO=${DO_YO:-1}
DO_BUILD=${DO_BUILD:-1}
DO_PUSH=${DO_PUSH:-1}
DO_YAML=${DO_YAML:-1}

POSSIBLE_IMAGES="alfresco solr6 share"


# ###########################################################################
# HELP

if [ "$1" == "-h" ] ; then
  echo "USAGE: $(basename $0) -h    // print help"
  echo "       $(basename $0) [-y]  // build images, -y skips confirmations"
  exit 0
fi

# ###########################################################################
# HELPERS

# $1 - command to check
check_command() {
  if ! command -v "$1" >/dev/null 2>&1 ; then
    echo "ERROR: command '$1' is not installed. Exiting."
    exit -1
  fi
}

# $1 - yaml file
# $2 - service name
# $3 - docker image
dc_set_image() {
  if yq --exit-status e ".services.$2" "$1" >/dev/null 2>&1 ; then
    echo -n "  Setting image for .services.$2 ... "
    yq -i e ".services.$2.image=\"$3\"" $1
    echo "done."
  else
    echo "  $1: No .services.$2, skipping."
  fi
}

# ###########################################################################
# SANITY CHECKS

check_command yo
check_command yq


# ###########################################################################
# GO

echo -e "\n\nExecuting installer with parameters:"
echo "  --acsVersion    ${ACS_VERSION}"
echo "  --ram           ${RAM}"
echo "  --mariadb       ${MARIADB}"
echo "  --crossLocale   ${CROSS_LOCALE}"
echo "  --smtp          ${SMTP}"
echo "  --ldap          ${LDAP}"
echo "  --addons        ${ADDONS}"
echo "  --startscript   ${START_SCRIPT}"
echo "  --https         ${HTTPS}"
echo "  --serverName    ${SERVER_NAME}"
echo "  --port          ${PORT}"
echo "  --ftp           ${FTP}"
echo "  --windows       ${WINDOWS}"
echo ""

if [ "$1" != "-y" ] ; then
  echo -e "\nHIT ENTER TO CONTINUE ..."
  read
fi


set -euo pipefail


if [ "$DO_CLEAN" == "1" ] ; then
  echo -e "\nCleaning old output files ..."
  ./clean.sh
else
  echo -e "\n\$DO_CLEAN != 1, not cleaning existing files."
fi
echo ""


if [ "$DO_NPM" == "1" ] ; then
  if ! test -d node_modules ; then
    echo -e "\nInstalling generator-alfresco-docker-installer ..."
    npm install generator-alfresco-docker-installer
  else
    echo -e "\nUpgrading generator-alfresco-docker-installer ..."
    npm upgrade generator-alfresco-docker-installer
  fi
else
  echo -e "\n\$DO_NPM != 1, not updating generator."
fi
echo ""


if [ "$DO_YO" == "1" ] ; then
  echo -e "\nRunning yeoman compose generator ..."
  yo alfresco-docker-installer \
    "--acsVersion=${ACS_VERSION}" \
    "--ram=${RAM}" \
    "--mariadb=${MARIADB}" \
    "--crossLocale=${CROSS_LOCALE}" \
    "--smtp=${SMTP}" \
    "--ldap=${LDAP}" \
    "--addons=${ADDONS}" \
    "--startscript=${START_SCRIPT}" \
    "--https=${HTTPS}" \
    "--serverName=${SERVER_NAME}" \
    "--port=${PORT}" \
    "--ftp=${FTP}" \
    "--windows=${WINDOWS}" \

else
  echo -e "\n\$DO_YO != 1, not running generator."
fi
echo ""


if [ "$DO_BUILD" == "1" ] ; then
  echo -e "\nBuilding docker images using docker-compose ..."
  docker-compose build
else
  echo -e "\n\$DO_BUILD != 1, not building containers."
fi
echo ""


IMAGE_BASE=$(basename "$PWD")
echo -e "\nChecking for built images ..."
for image in $POSSIBLE_IMAGES ; do
  BUILT_IMAGE=$(docker images | grep _$image | cut -f1 -d" ")
  REPO=${REPOS[$image]}
  echo "  $BUILT_IMAGE --> $REPO"
done
echo ""

if [ "$DO_PUSH" == "1" ] ; then
  echo -e "\nPushing images ..."
  for image in $POSSIBLE_IMAGES ; do
      docker tag $BUILT_IMAGE $REPO:latest
      docker tag $BUILT_IMAGE $REPO:$TIMESTAMP

      docker push $REPO:latest
      docker push $REPO:$TIMESTAMP
  done
else
  echo -e "\n\$DO_PUSH != 1, not pushing images."
fi
echo ""

if [ "$DO_YAML" == "1" ] ; then
  echo -e "\nSetting container images in docker-compose.yml ... "
  yq e docker-compose.yml > dcu.yml
  cp dcu.yml dcw.yml
  yq -i e 'del(.. | select(has("build")).build)' dcw.yml

  for image in $POSSIBLE_IMAGES ; do
    dc_set_image dcw.yml "$image" "${REPOS[$image]}:$TIMESTAMP"
  done

  mv -f docker-compose.yml docker-compose.yml.generated
  mv -f dcu.yml docker-compose.yml.unmodified
  mv -f dcw.yml docker-compose.yml
else
  echo -e "\n\$DO_YAML != 1, not processing docker-compose.yml.\n"
fi
echo ""
