#!/usr/bin/env bash

# $@ - thing to delete ...
delme() {
  for delete_me in "$@" ; do
    echo -n "  Deleting $delete_me ... "
    rm -rf "$delete_me"
    echo "done."
  done
}

echo -e "\nCleaning old data ..."
delme alfresco
delme config
delme search
delme share
delme .env
delme start.sh
delme docker-compose.yml*

if [ "$1" == "-a" ] ; then
  echo -e "\nCleaning *all* files ..."
  delme node_modules
  delme package*
fi

echo ""
